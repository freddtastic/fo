package se.fredde.FO;

import java.util.List;

public class FOClan {

    Integer id;
    String name;
    FOPlayer foPlayer;
    List<FOPlayer> foPlayers;

    public FOClan(Integer id, String name, FOPlayer foPlayer, List<FOPlayer> foPlayers) {
        this.id = id;
        this.name = name;
        this.foPlayer = foPlayer;
        this.foPlayers = foPlayers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FOPlayer getFoPlayer() {
        return foPlayer;
    }

    public void setFoPlayer(FOPlayer foPlayer) {
        this.foPlayer = foPlayer;
    }

    public List<FOPlayer> getFoPlayers() {
        return foPlayers;
    }

    public void setFoPlayers(List<FOPlayer> foPlayers) {
        this.foPlayers = foPlayers;
    }

}
