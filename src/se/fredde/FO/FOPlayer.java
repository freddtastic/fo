package se.fredde.FO;

import java.util.UUID;

public class FOPlayer {

    UUID uuid;
    FOClan foClan;

    public FOPlayer(UUID uuid, FOClan foClan) {
        this.uuid = uuid;
        this.foClan = foClan;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public FOClan getFoClan() {
        return foClan;
    }

    public void setFoClan(FOClan foClan) {
        this.foClan = foClan;
    }

}
