package se.fredde.FO;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class FOAreaEvent implements Listener {

    JavaPlugin plugin;

    FOChat foChat;
    FOCheck foCheck;
    List<FOArea> foAreas;
    List<FOClan> foClans;
    List<FOPlayer> foPlayers;

    public FOAreaEvent(JavaPlugin plugin, FOChat foChat, FOCheck foCheck, List<FOArea> foAreas, List<FOClan> foClans, List<FOPlayer> foPlayers) {
        this.plugin = plugin;
        this.foChat = foChat;
        this.foCheck = foCheck;
        this.foAreas = foAreas;
        this.foClans = foClans;
        this.foPlayers = foPlayers;
    }

}
