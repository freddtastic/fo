package se.fredde.FO;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class FOArea {

    JavaPlugin plugin;

    Integer id;
    String name;
    FOClan foClan;
    World world;
    Vector p1, p2, nexus;

    public FOArea(JavaPlugin plugin, Integer id, String name, FOClan foClan, World world, Vector p1, Vector p2, Vector nexus) {
        this.plugin = plugin;
        this.id = id;
        this.name = name;
        this.foClan = foClan;
        this.world = world;
        int x1 = Math.min(p1.getBlockX(), p2.getBlockX());
        int y1 = Math.min(p1.getBlockY(), p2.getBlockY());
        int z1 = Math.min(p1.getBlockZ(), p2.getBlockZ());
        int x2 = Math.max(p1.getBlockX(), p2.getBlockX());
        int y2 = Math.max(p1.getBlockY(), p2.getBlockY());
        int z2 = Math.max(p1.getBlockZ(), p2.getBlockZ());
        this.p1 = new Vector(x1, y1, z1);
        this.p2 = new Vector(x2, y2, z2);
        this.nexus = nexus;
        world.getBlockAt(nexus.toLocation(world)).setType(Material.EMERALD_BLOCK);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FOClan getFoClan() {
        return foClan;
    }

    public void setFoClan(FOClan foClan) {
        this.foClan = foClan;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Vector getP1() {
        return p1;
    }

    public void setP1(Vector p1) {
        this.p1 = p1;
    }

    public Vector getP2() {
        return p2;
    }

    public void setP2(Vector p2) {
        this.p2 = p2;
    }

    public Vector getNexus() {
        return nexus;
    }

    public void setNexus(Vector nexus) {
        this.nexus = nexus;
    }

}
