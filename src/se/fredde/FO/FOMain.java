package se.fredde.FO;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class FOMain extends JavaPlugin {

    Logger logger;

    FOChat foChat;
    FOCheck foCheck;
    List<FOArea> foAreas;
    List<FOClan> foClans;
    List<FOPlayer> foPlayers;

    @Override
    public void onEnable() {
        logger = getLogger();
        logger.info("Booting...");
        foAreas = new ArrayList<>();
        foClans = new ArrayList<>();
        foPlayers = new ArrayList<>();
        logger.info("Lists loaded.");
        foCheck = new FOCheck(foAreas, foClans, foPlayers);
        logger.info("Checks loaded.");
        foChat = new FOChat();
        logger.info("Chat loaded.");
        getServer().getPluginManager().registerEvents(new FOAreaEvent(this, foChat, foCheck, foAreas, foClans, foPlayers), this);
        getServer().getPluginManager().registerEvents(new FOClanEvent(this, foChat, foCheck, foAreas, foClans, foPlayers), this);
        getServer().getPluginManager().registerEvents(new FOPlayerEvent(this, foChat, foCheck, foAreas, foClans, foPlayers), this);
        logger.info("Events loaded.");
        getCommand("area").setExecutor(new FOAreaCommand());
        getCommand("clan").setExecutor(new FOClanCommand());
        getCommand("me").setExecutor(new FOPlayerCommand());
        logger.info("Commands loaded.");
        logger.info("Successfully booted.");
    }

}
