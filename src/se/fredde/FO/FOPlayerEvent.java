package se.fredde.FO;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.UUID;

public class FOPlayerEvent implements Listener {

    JavaPlugin plugin;

    FOChat foChat;
    FOCheck foCheck;
    List<FOArea> foAreas;
    List<FOClan> foClans;
    List<FOPlayer> foPlayers;

    PotionEffect EFFECT_ENEMY_ENTER = new PotionEffect(PotionEffectType.SLOW_DIGGING, 400, 0);
    PotionEffect EFFECT_MEMBER_ENTER = new PotionEffect(PotionEffectType.SPEED, 200, 0);

    PotionEffect EFFECT_ENEMY_CAPTURE = new PotionEffect(PotionEffectType.SLOW_DIGGING, 20, 0);
    PotionEffect EFFECT_MEMBER_CAPTURE1 = new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 200, 0);
    PotionEffect EFFECT_MEMBER_CAPTURE2 = new PotionEffect(PotionEffectType.SPEED, 200, 0);
    PotionEffect EFFECT_MEMBER_CAPTURE3 = new PotionEffect(PotionEffectType.JUMP, 200, 0);

    public FOPlayerEvent(JavaPlugin plugin, FOChat foChat, FOCheck foCheck, List<FOArea> foAreas, List<FOClan> foClans, List<FOPlayer> foPlayers) {
        this.plugin = plugin;
        this.foChat = foChat;
        this.foCheck = foCheck;
        this.foAreas = foAreas;
        this.foClans = foClans;
        this.foPlayers = foPlayers;
    }

    @EventHandler
    public void Login(PlayerLoginEvent e) {
        Player player = e.getPlayer();
        UUID uuidPlayer = player.getUniqueId();
        if (foCheck.isPlayerNew(player)) {
            foPlayers.add(new FOPlayer(player.getUniqueId(), null));
        }
        player.setGameMode(GameMode.ADVENTURE);
        // Tests
        FOPlayer foPlayer = foCheck.getPlayerByUuid(uuidPlayer);
        foAreas.add(new FOArea(plugin, 0, "Lorem", null, player.getWorld(), new Vector(0, 4, 0), new Vector(200, 90, 200), new Vector(20, 5, 20)));
    }

    @EventHandler
    public void Move(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        UUID uuidPlayer = player.getUniqueId();
        Location locationFrom = e.getFrom();
        Location locationTo = e.getTo();
        if (locationFrom.getBlockX() != locationTo.getBlockX()
                || locationFrom.getBlockY() != locationTo.getBlockY()
                || locationFrom.getBlockZ() != locationTo.getBlockZ()) {
            if (foCheck.isLocationInsideArea(locationTo) && !foCheck.isLocationInsideArea(locationFrom)) {
                // isPlayerInsideArea
                FOPlayer foPlayer = foCheck.getPlayerByUuid(uuidPlayer);
                if (foPlayer == null) {
                    foPlayers.add(new FOPlayer(uuidPlayer, null));
                }
                if (foCheck.isPlayerInClan(foPlayer)) {
                    // isPlayerInClan
                    player.setGameMode(GameMode.SURVIVAL);
                    FOArea foArea = foCheck.getAreaByLocation(locationTo);
                    player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                    player.sendMessage("");
                    player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&aEntered &l" + foArea.getName() + "&r&a."));
                    if (foCheck.isAreaCaptured(foArea)) {
                        // isAreaCaptured
                        if (foCheck.isPlayerMemberOfArea(foPlayer, foArea)) {
                            // isMember
                            player.addPotionEffect(EFFECT_MEMBER_ENTER);
                            player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&aClan: " + foArea.getFoClan().getName()));
                            player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&aOwner: " + plugin.getServer().getOfflinePlayer(foArea.getFoClan().getFoPlayer().getUuid()).getName()));
                        } else {
                            // isNotMember
                            player.addPotionEffect(EFFECT_ENEMY_ENTER);
                            player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&cClan: " + foArea.getFoClan().getName()));
                            player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&cOwner: " + plugin.getServer().getOfflinePlayer(foArea.getFoClan().getFoPlayer().getUuid()).getName()));
                        }
                    } else {
                        player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&7Area is not captured."));
                    }
                    player.sendMessage("");
                    player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                } else {
                    // isNotPlayerInClan
                    player.teleport(locationFrom);
                    player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + "&cYou need to be in a clan."));
                }
            } else if (!foCheck.isLocationInsideArea(locationTo) && foCheck.isLocationInsideArea(locationFrom)) {
                // isNotPlayerInsideArea
                FOArea foArea = foCheck.getAreaByLocation(locationFrom);
                player.setGameMode(GameMode.ADVENTURE);
                player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                player.sendMessage("");
                player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&cExited &l" + foArea.getName() + "&r&c."));
                player.sendMessage("");
                player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
            }
        }
    }

    @EventHandler
    public void BlockDamage(BlockDamageEvent e) {
        Player player = e.getPlayer();
        UUID uuidPlayer = player.getUniqueId();
        Location locationPlayer = player.getLocation();
        Block block = e.getBlock();
        Location locationBlock = block.getLocation();
        if (block.getType().equals(Material.EMERALD_BLOCK)) {
            if (foCheck.isLocationInsideArea(locationPlayer)) {
                // isPlayerInsideArea
                FOPlayer foPlayer = foCheck.getPlayerByUuid(uuidPlayer);
                if (foPlayer == null) {
                    foPlayers.add(new FOPlayer(uuidPlayer, null));
                }
                if (foCheck.isPlayerInClan(foPlayer)) {
                    // isPlayerInClan
                    if (foCheck.isLocationInsideArea(locationBlock)) {
                        // isBlockInsideArea
                        FOArea foArea = foCheck.getAreaByLocation(locationBlock);
                        if (foCheck.isLocationNexus(locationBlock, foArea)) {
                            // isLocationNexus
                            if (foCheck.isAreaCaptured(foArea)) {
                                // isAreaCaptured
                                if (foCheck.isPlayerMemberOfArea(foPlayer, foArea)) {
                                    // isPlayerMemberOfArea
                                    player.addPotionEffect(EFFECT_MEMBER_CAPTURE1);
                                    player.addPotionEffect(EFFECT_MEMBER_CAPTURE2);
                                    player.addPotionEffect(EFFECT_MEMBER_CAPTURE3);
                                    player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                    player.sendMessage("");
                                    player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&aYou got buffed."));
                                    player.sendMessage("");
                                    player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                    e.setCancelled(true);
                                } else {
                                    // isNotPlayerMemberOfArea
                                    player.addPotionEffect(EFFECT_ENEMY_CAPTURE);
                                    for (FOPlayer member : foCheck.getPlayerByArea(foArea)) {
                                        if (plugin.getServer().getPlayer(member.getUuid()) != null) {
                                            Player target = plugin.getServer().getPlayer(member.getUuid());
                                            target.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                            target.sendMessage("");
                                            target.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&cSomeone is trying to capture &l" + foArea.getName() + "&r&c!"));
                                            target.sendMessage("");
                                            target.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // isNotBlockInsideArea
                        e.setCancelled(true);
                    }
                } else {
                    // isNotPlayerInClan
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void BlockBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        UUID uuidPlayer = player.getUniqueId();
        Location locationPlayer = player.getLocation();
        Block block = e.getBlock();
        Location locationBlock = block.getLocation();
        if (block.getType().equals(Material.EMERALD_BLOCK)) {
            if (foCheck.isLocationInsideArea(locationPlayer)) {
                // isPlayerInsideArea
                FOPlayer foPlayer = foCheck.getPlayerByUuid(uuidPlayer);
                if (foPlayer == null) {
                    foPlayers.add(new FOPlayer(uuidPlayer, null));
                }
                if (foCheck.isPlayerInClan(foPlayer)) {
                    // isPlayerInClan
                    if (foCheck.isLocationInsideArea(locationBlock)) {
                        // isBlockInsideArea
                        FOArea foArea = foCheck.getAreaByLocation(locationBlock);
                        if (foCheck.isLocationNexus(locationBlock, foArea)) {
                            // isLocationNexus
                            if (foCheck.isAreaCaptured(foArea)) {
                                if (!foCheck.isPlayerMemberOfArea(foPlayer, foArea)) {
                                    // isNotPlayerMemberOfArea
                                    player.addPotionEffect(EFFECT_MEMBER_CAPTURE1);
                                    player.addPotionEffect(EFFECT_MEMBER_CAPTURE2);
                                    player.addPotionEffect(EFFECT_MEMBER_CAPTURE3);
                                    foArea.setFoClan(foCheck.getClanByMember(foPlayer));
                                    player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                    player.sendMessage("");
                                    player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&aYou captured &l" + foArea.getName() + "&r&a!"));
                                    player.sendMessage("");
                                    player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                    plugin.getServer().broadcastMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + "&a" + player.getName() + " captured &l" + foArea.getName() + "&r&a."));
                                    e.setCancelled(true);
                                }
                            } else {
                                // isNotAreaCaptured
                                player.addPotionEffect(EFFECT_MEMBER_CAPTURE1);
                                player.addPotionEffect(EFFECT_MEMBER_CAPTURE2);
                                player.addPotionEffect(EFFECT_MEMBER_CAPTURE3);
                                foArea.setFoClan(foCheck.getClanByMember(foPlayer));
                                player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                player.sendMessage("");
                                player.sendMessage(foChat.cc(foChat.CHAT_SPACE + "&aYou captured &l" + foArea.getName() + "&r&a!"));
                                player.sendMessage("");
                                player.sendMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + foChat.CHAT_DIVIDER));
                                plugin.getServer().broadcastMessage(foChat.cc(foChat.CHAT_PREFIX + foChat.CHAT_SPACE + "&a" + player.getName() + " captured &l" + foArea.getName() + "&r&a."));
                                e.setCancelled(true);
                            }
                        }

                    } else {
                        // isNotBlockInsideArea
                        e.setCancelled(true);
                    }
                } else {
                    // isNotPlayerInClan
                    e.setCancelled(true);
                }
            }
        }
        if (foCheck.isLocationInsideArea(locationPlayer)) {
            if (!foCheck.isLocationInsideArea(locationBlock)) {
                e.setCancelled(true);
            }
        }
    }

}
