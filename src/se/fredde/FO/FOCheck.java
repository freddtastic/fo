package se.fredde.FO;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FOCheck {

    List<FOArea> foAreas;
    List<FOClan> foClans;
    List<FOPlayer> foPlayers;

    public FOCheck(List<FOArea> foAreas, List<FOClan> foClans, List<FOPlayer> foPlayers) {
        this.foAreas = foAreas;
        this.foClans = foClans;
        this.foPlayers = foPlayers;
    }

    // FOArea

    public FOArea getAreaById(Integer id) {
        for (FOArea foArea : foAreas) {
            if (foArea.getId().equals(id)) {
                return foArea;
            }
        }
        return null;
    }

    public List<FOArea> getAreaByClan(FOClan foClan) {
        List<FOArea> areas = new ArrayList<>();
        for (FOArea foArea : foAreas) {
            if (foArea.getFoClan().equals(foClan)) {
                areas.add(foArea);
            }
        }
        return areas;
    }

    public List<FOArea> getAreaByOwner(FOPlayer foPlayer) {
        List<FOArea> areas = new ArrayList<>();
        for (FOArea foArea : foAreas) {
            if (foArea.getFoClan().getFoPlayer().equals(foPlayer)) {
                areas.add(foArea);
            }
        }
        return areas;
    }

    public List<FOArea> getAreaByMember(FOPlayer foPlayer) {
        List<FOArea> areas = new ArrayList<>();
        for (FOArea foArea : foAreas) {
            for (FOPlayer member : foArea.getFoClan().getFoPlayers()) {
                if (member.equals(foPlayer)) {
                    areas.add(foArea);
                }
            }
        }
        return areas;
    }

    public FOArea getAreaByLocation(Location location) {
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        for (FOArea foArea : foAreas) {
            Vector p1 = foArea.getP1();
            Vector p2 = foArea.getP2();
            if (x >= p1.getBlockX() && x <= p2.getBlockX()
                    && y >= p1.getBlockY() && y <= p2.getBlockY()
                    && z >= p1.getBlockZ() && z <= p2.getBlockZ()) {
                return foArea;
            }
        }
        return null;
    }

    // FOClan

    public FOClan getClanById(Integer id) {
        for (FOClan foClan : foClans) {
            if (foClan.getId().equals(id)) {
                return foClan;
            }
        }
        return null;
    }

    public FOClan getClanByOwner(FOPlayer foPlayer) {
        for (FOClan foClan : foClans) {
            if (foClan.getFoPlayer().equals(foPlayer)) {
                return foClan;
            }
        }
        return null;
    }

    public FOClan getClanByMember(FOPlayer foPlayer) {
        for (FOClan foClan : foClans) {
            for (FOPlayer member : foClan.getFoPlayers()) {
                if (member.equals(foPlayer)) {
                    return foClan;
                }
            }
        }
        return null;
    }

    // FOPlayer

    public FOPlayer getPlayerByUuid(UUID uuid) {
        for (FOPlayer foPlayer : foPlayers) {
            if (foPlayer.getUuid().equals(uuid)) {
                return foPlayer;
            }
        }
        return null;
    }

    public List<FOPlayer> getPlayerByClan(FOClan foClan) {
        return foClan.getFoPlayers();
    }

    public List<FOPlayer> getPlayerByArea(FOArea foArea) {
        if (foArea.getFoClan() != null) {
            return foArea.getFoClan().getFoPlayers();
        }
        return null;
    }

    public boolean isPlayerNew(Player player) {
        for (FOPlayer foPlayer : foPlayers) {
            if (foPlayer.getUuid().equals(player.getUniqueId())) {
                return false;
            }
        }
        return true;
    }

    public boolean isPlayerInClan(FOPlayer foPlayer) {
        return foPlayer.getFoClan() != null;
    }

    public boolean isPlayerOwnerOfClan(FOPlayer foPlayer, FOClan foClan) {
        return foPlayer.equals(foClan.getFoPlayer());
    }

    public boolean isPlayerMemberOfClan(FOPlayer foPlayer, FOClan foClan) {
        for (FOPlayer member : foClan.getFoPlayers()) {
            if (member.equals(foPlayer)) {
                return true;
            }
        }
        return false;
    }

    public boolean isPlayerOwnerOfArea(FOPlayer foPlayer, FOArea foArea) {
        return foPlayer.equals(foArea.getFoClan().getFoPlayer());
    }

    public boolean isPlayerMemberOfArea(FOPlayer foPlayer, FOArea foArea) {
        for (FOPlayer member : foArea.getFoClan().getFoPlayers()) {
            if (member.equals(foPlayer)) {
                return true;
            }
        }
        return false;
    }

    public boolean isLocationInsideArea(Location location) {
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        for (FOArea foArea : foAreas) {
            Vector p1 = foArea.getP1();
            Vector p2 = foArea.getP2();
            if (x >= p1.getBlockX() && x <= p2.getBlockX()
                    && y >= p1.getBlockY() && y <= p2.getBlockY()
                    && z >= p1.getBlockZ() && z <= p2.getBlockZ()) {
                return true;
            }
        }
        return false;
    }

    public boolean isLocationNexus(Location location, FOArea foArea) {
        Vector nexus = foArea.getNexus();
        int x = nexus.getBlockX();
        int y = nexus.getBlockY();
        int z = nexus.getBlockZ();
        Location locationNexus = new Location(location.getWorld(), x, y, z);
        return location.equals(locationNexus);
    }

    public boolean isAreaCaptured(FOArea foArea) {
        return foArea.getFoClan() != null;
    }

}
